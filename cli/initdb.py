#!/usr/bin/env python
import logging
import time
from datetime import date, timedelta
import os
import sys

import click
import redis

# python workaround importing from parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from utils.feedcup import feed_dir
from utils.device import download_devices
from utils.db import Db
import setting


@click.command()
@click.option('-de', '--delete-events', type=int,
              help='delete events previously to a number of days, preserve non null code')
@click.option('-def', '--delete-events-force', type=int,
              help='delete events previously to a number of days')
@click.option('-ape', '--append-events', type=click.Path(exists=True),
              help='append events from another db specified by path name')
@click.option('-acs', '--adding-cups', help='adding all cups files', is_flag=True)
@click.option('-ac', '--adding-cup', help='adding specific cup file')
@click.option('-ad', '--adding-devices', help='adding devices', is_flag=True)
@click.option('-dd', '--delete-devices', help='delete devices', is_flag=True)
@click.option('-rc', '--red-clean', help='redis clean old streams', is_flag=True)
@click.option('-rca', '--red-clean-all', help='redis clean all', is_flag=True)
def main(delete_events, delete_events_force, append_events, adding_cups,
         adding_cup, adding_devices, delete_devices, red_clean, red_clean_all):

    logging.basicConfig(level=logging.WARNING)
    db = Db(path=setting.SQLITE_PATH)
    red = redis.Redis(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                      decode_responses=True, encoding="utf-8")
    try:
        red.ping()
    except BaseException as e:
        print("Error while testing redis connection")
        print("Check redis server is up and running")
        logging.debug(str(e))
        exit(1)

    if delete_events:
        date_ = subdate(delete_events)
        print("delete null events for date before {}".format(date_))
        db.delete_events(before=date_)
        db.vacuum()

    if delete_events_force:
        date_ = subdate(delete_events_force)
        print("delete events for date before {}".format(date_))
        db.delete_events(before=date_, null_=False)
        db.vacuum()

    if append_events:
        db_new_path = click.format_filename(append_events)
        if db_new_path == db.path:
            print("sqlite db paths must be different: {}".format(db.path))
            return
        db_new = Db(path=db_new_path)
        print("actual events base: {}".format(db.path))
        print("new events base: {}".format(db_new.path))
        answer = input("Append new events to actual base[yes/N]? ").lower()
        if answer != "yes":
            print("abort")
            return
        records = db_new.get_events_all()
        db.insert_event_lines(records=records)

    if adding_cups:
        print("adding airfields location and altitude in Redis Database and Sql Database")
        feed_dir(db=db, red=red)
        # TODO implement signal thread (or in main loop event) in consumer
        print("relaunch consumer(s) process for loading new data")

    if adding_cup:
        print("adding airfields location and altitude in Redis Database and Sql Database")
        feed_dir(db=db, red=red, filename=adding_cup)

    if adding_devices:
        print("Adding devices")
        download_devices(db=db, red=red)

    if delete_devices:
        print("Delete devices")
        red.delete('SIGN:OGN')
        db.delete_devices()

    if red_clean:
        print("Redis clean old path stream")
        redclean(red=red)

    if red_clean_all:
        print("Redis clean all")
        redclean(red=red, all_=True)


def subdate(nb):
    """
    sub a number of days to actual date and return a date format 'YYYY-MM-dd'
    :param nb: int numbers of days to sub
    :return: str
    """
    ret = date.today() - timedelta(days=nb)
    return ret


def redclean(red, all_=False):
    """
    Clean redis Stream older than 2 weeks
    :param red: redis client instance
    :param all_: boolean
    :return:
    """

    redis_info(red)
    call_time = int(time.time())
    idle_time = call_time - setting.PATH_RETENTION * 2      # 48 hours before
    idle_addr = set(red.zrangebyscore(name='SSA', min=0, max=idle_time))
    act_addr = set(red.zrangebyscore(name='SSA', min='({}'.format(idle_time), max='+inf'))

    print("{} IDLE addresses and {} Actives addresses found".format(len(idle_addr), len(act_addr)))
    print("{} IDLE Stream Path SA:* will be delete".format(len(idle_addr)))
    stream_paths = list(map(lambda x: 'SA:{}'.format(x), idle_addr))
    # print(stream_paths)
    if stream_paths:
        pass
        red.delete(*stream_paths)
        red.zrem('SSA', *idle_addr)
        print("...Done")
    if not all_:
        redis_info(red)
        return

    all_addr = set(map(lambda x: x[3:], red.keys(pattern="BA:*")))
    rem_addr = all_addr - act_addr
    print("{} IDLE addresses on {} total addresses found".format(len(rem_addr), len(all_addr)))
   
    answer = input("Remove {} BA:* keys [yes/N]? ".format(len(rem_addr))).lower()
    if answer != "yes":
        print("Abort removing BA:* keys")
    else:
        ba_keys = list(map(lambda x: 'BA:{}'.format(x), rem_addr))
        if ba_keys:
            red.delete(*ba_keys)
   
    answer = input("Remove {} STATE members [yes/N]? ".format(len(rem_addr))).lower()
    if answer != "yes":
        print("Abort removing STATE members")
    else:
        if rem_addr:
            red.hdel('STATE', *rem_addr)
    
    answer = input("Remove {} GEO:A members [yes/N]? ".format(len(rem_addr))).lower()
    if answer != "yes":
        print("Abort removing GEO:A members")
    else:
        if rem_addr:
            red.zrem('GEO:A', *rem_addr)

    answer = input("Remove {} FLEET members [yes/N]? ".format(len(rem_addr))).lower()
    # TODO FLEET cleaner
    redis_info(red)


def redis_info(red):
    """
    Print useful redis metrics
    :param red: a redis conection object
    :return:
    """
    red_info = red.info()
    used_memory = red_info['used_memory']
    used_memory_human = red_info['used_memory_human']
    used_memory_rss = red_info['used_memory_rss']
    used_memory_rss_human = red_info['used_memory_rss_human']
    frag_ratio = round(used_memory_rss / used_memory, 4)
    print("used_memory: {}, memory_human: {}\n"
          "used_memory_rss: {} , memory_human_rss: {}\nfragmentation ratio: {}".
          format(used_memory, used_memory_human, used_memory_rss, used_memory_rss_human, frag_ratio))


if __name__ == "__main__":
    main()
