package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

/*
Longest message are around 117 strings characts => 468 bytes
Optimize BufferSize will be 512 bytes.
Adapt buffer size decrease memory footprint on go routine
*/
var upgrader = websocket.Upgrader{
	ReadBufferSize:  512,
	WriteBufferSize: 512,
}

func Upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	// delegate upgrader CheckOrigin to reverse
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return conn, nil
}
